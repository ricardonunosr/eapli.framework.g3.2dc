/**
 *
 */
package eapli.framework.infrastructure.eventpubsub.impl.simplepersistent;

import eapli.framework.domain.repositories.Repository;

/**
 * @author pgsou_000
 *
 */
interface EventConsumptionRepository extends Repository<EventConsumption, Long> {

}
