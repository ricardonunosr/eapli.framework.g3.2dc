package eapli.framework.infrastructure.eventpubsub.impl.simplepersistent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eapli.framework.actions.TimedActions;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import eapli.framework.infrastructure.eventpubsub.EventHandler;
import eapli.framework.infrastructure.eventpubsub.EventPublisher;
import eapli.framework.infrastructure.eventpubsub.impl.inprocess.InProcessEventDispatcher;
import eapli.framework.infrastructure.eventpubsub.impl.inprocess.InProcessEventPublisher;
import eapli.framework.util.Singleton;

/**
 * A simple event dispatcher using database tables to persist events and allow
 * for out of process publish-subscribe.
 *
 * This class is just for demonstration purposes. you should consider using a
 * true pub/sub mechanism such as message queueing instead of this class.
 *
 * This class is only prepared to be used by Spring Dependency Injection
 *
 * @author pgsou_000
 *
 */
@Component
public class SimplePersistentEventPubSub implements EventDispatcher, EventPublisher, Singleton {

    private static final Logger LOGGER = LogManager.getLogger(SimplePersistentEventPubSub.class);

    @Autowired
    private EventRecordRepository eventRepo;
    @Autowired
    private EventConsumptionRepository consumptionRepo;

    private final EventDispatcher inprocDispatcher = InProcessEventDispatcher.instance();
    private final EventPublisher inprocPublisher = InProcessEventPublisher.instance();

    /**
     * identify the consumer application instance. useful if you have multiple
     * instances of the same application and want to dispatch events to all of
     * them
     */
    private final String instanceKey;
    /**
     * interval (in milliseconds) between poolings to the event store
     */
    private final int poolInterval;

    private volatile boolean timerInitialized;

    /**
     *
     * @param instanceKey
     *            the application instance.
     * @param poolInterval
     *            interval (in milliseconds) between poolings to the event store
     */
    protected SimplePersistentEventPubSub(final String instanceKey, final int poolInterval) {
        this.instanceKey = instanceKey;
        this.poolInterval = poolInterval;
    }

    private void storeConsumptionAndPublish(final EventRecord record) {
        final EventConsumption entity = new EventConsumption(instanceKey, record);
        try {
            consumptionRepo.save(entity);
            LOGGER.trace("Stored event consumption {}", entity);
            inprocPublisher.publish(record.event());
        } catch (ConcurrencyException | IntegrityViolationException e) {
            LOGGER.error("Error storing consumption {} of event {}", entity, record, e);
        }
    }

    private boolean pollEvents() {
        LOGGER.trace("Polling events from database");
        try {
            final Iterable<EventRecord> notConsumedEvents = eventRepo.findNotConsumed(instanceKey);
            notConsumedEvents.forEach(this::storeConsumptionAndPublish);
        } catch (final Exception e) {
            LOGGER.error("Error polling events", e);
        }
        return true;
    }

    private void initTimersIfNeeded() {
        if (!timerInitialized) {
            TimedActions.atFixedRate(this::pollEvents, poolInterval);
            timerInitialized = true;
        }
    }

    @Override
    public void publish(final DomainEvent event) {
        initTimersIfNeeded();

        storeEvent(event);

        // we do not immediately publish the event in process as it will be
        // polled and marked as consumed soon enough
    }

    private void storeEvent(final DomainEvent event) {
        final EventRecord record = new EventRecord(instanceKey, event);
        try {
            eventRepo.save(record);
            LOGGER.trace("Stored event {}", event);
        } catch (ConcurrencyException | IntegrityViolationException e) {
            LOGGER.error("Error storing event {}", event, e);
        }
    }

    @Override
    public void subscribe(final EventHandler observer, final Class<? extends DomainEvent>... events) {
        initTimersIfNeeded();

        inprocDispatcher.subscribe(observer, events);
    }

    @Override
    public void unsubscribe(final EventHandler observer, final Class<? extends DomainEvent>... events) {
        inprocDispatcher.unsubscribe(observer, events);
    }

    @Override
    public void unsubscribe(final EventHandler observer) {
        inprocDispatcher.unsubscribe(observer);
    }

    @Override
    public void shutdown() {
        TimedActions.shutdownRecurringTasks();
        LOGGER.debug("Shuting down the event dispatcher");
    }
}
