/**
 *
 */
package eapli.framework.domain.model.math.numeral;

/**
 * @author sou03408
 *
 */
public class OctalNumeralTest extends NumeralTestBase {

    @Override
    protected int getBase() {
        return 8;
    }
}
